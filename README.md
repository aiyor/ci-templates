# Gitlab CI Templates

This repository holds CI templates that aim to simplify commonly used workflows.

**Note:**

*Some templates make use of `spec:inputs`, which is a beta feature in Gitlab. Refer to https://docs.gitlab.com/ee/ci/yaml/includes.html#define-inputs-for-configuration-added-with-include-beta for more information.*


## Repository Structure

Each category/group of templates is stored in its own directory.  Each category/group can have one or more templates.

There should be directory named `examples` within each category/group that provide example(s) on how to use the templates.

## Reference to the Templates Repository

As this repository is meant to be a shared/centralised templates, the way to use templates here is via `include:` in the calling CI, e.g.,

```yaml
include:
  - project: aiyor/ci-templates
    ref: main
    file: wif/common.yml
```
